// Is case sensitive
// By convention we should use camelCase
// single line comment 
/* This is a multi-line comment */

let sum = 10 + 20    // valid even without a semicolon - not recommended 
let diff = 20 - 10;  // valid - preferred 

/* Reserved words:
break       do          in          typeof 
case        else        instanceof  var 
catch       export      new         void 
class       extends     return      while 
const       finally     super       with 
continue    for         switch      yield 
debugger    function    this        default     
if          throw delete      import      try 
*/

//Variables

var message = "hi"; 
message = 100;  // legal, but not recommended

// var scope

function testOne() {  
  var messageOne = "hi";  // local variable 
} 
testOne(); 
//console.log(messageOne); // error! 


function testTwo() {  
  messageTwo = "hi";  // global variable 
} 
testTwo(); 
console.log(messageTwo); // "hi"

// define more than one variable

var messageThree = "hi", found = false, age = 29;

// var Declaration Hoisting 

function foo() {  
  console.log(ageOne);  
  var ageOne = 26; 
} 
foo();  // undefined 

// let declarations - let is block scoped, but var is function scoped

if (true) {
  var name = 'Matt'; 
  console.log(name);  // Matt 
} 
console.log(name);    // Matt

if (true) { 
  let ageTwo = 26; 
  console.log(ageTwo);   // 26 
} 
//console.log(ageTwo);     // ReferenceError: ageTwo is not defined

var nameOne; 
var nameOne;
let ageThree; 
//let ageThree;  // SyntaxError; identifier 'age' has already been declared

var nameTwo = 'Nicholas'; 
console.log(nameTwo);  // 'Nicholas' 
if (true) {  
  var nameTwo = 'Matt';  
  console.log(nameTwo);  // 'Matt' 
}
 
let ageFour = 30; 
console.log(ageFour);  // 30 
if (true) {  
  let ageFour = 26;  
  console.log(ageFour);  // 26 
}

// Temporal dead zone

// name is hoisted 
console.log(nameThree);  // undefined 
var nameThree = 'Matt';
 
// age is not hoisted 
//console.log(ageFive);  // ReferenceError: ageFive is not defined 
let ageFive = 26; 

// Global Declarations - in Node doesnt exist the window object

var nameFour = 'Matt'; 
//console.log(window.nameFour);  // 'Matt' 
 
let ageSix = 26; 
//console.log(window.ageSix);   // undefined 


// let Declaration in for Loops

for (var i = 0; i < 5; ++i) {  
  // do loop things 
} 
console.log(i);  // 5 

for (let i = 0; i < 5; ++i) {  
  // do loop things 
} 
//console.log(i);  // ReferenceError: i is not defined 

for (var j = 0; j < 4; ++j) {  
  setTimeout(() => console.log(j), 1) 
}

for (let k = 0; k < 4; ++k) {  
  setTimeout(() => console.log(k),1) 
} 


//const Declarations

const ageConst = 26; 
//ageConst = 36;  // TypeError: assignment to a constant

// const still disallows redundant declaration 
const nameConst = 'Matt'; 
//const nameConst = 'Nicholas';  // SyntaxError
 
// const is still scoped to blocks 
const nameConstOne = 'Matt'; 
if (true) {  
  const nameConstOne = 'Nicholas'; 
} 
console.log(nameConstOne);  // Matt 


const person = {}; 
person.name = 'Matt';  // ok 

// for 
//for (const w = 0; w < 10; ++w) {}  // TypeError: assignment to constant variable

for (const key in {a: 1, b: 2}) {  
  console.log(key); 
}

for (const value of [1,2,3,4,5]) {  
  console.log(value); 
}

// Declaration Styles and Best Practices 
// Don’t Use var
// Prefer const Over let 


// Data Types
// Undefined, Null, Boolean, Number, String, and Symbol. Symbol

// The typeof Operator

let typeOfExample = "some string"; 
console.log(typeof typeOfExample);   // "string" 
console.log(typeof(typeOfExample));  // "string" 
console.log(typeof 95);        // "number"

//The Undefined Type - Generally speaking, you should never explicitly set a variable to be undefined

let messageUndefined; 
console.log(messageUndefined == undefined);  // true 

let messageUndefinedTwo; 
console.log(messageUndefinedTwo == undefined);  // true 

let messageUndefinedThree;   // this variable is declared but has a value of undefined           
// make sure this variable isn't declared 
// let ageUndefined           
console.log(messageUndefinedThree);  // "undefined" 
//console.log(ageUndefined);      // causes an error


let messageUndefinedFour;   // this variable is declared but has a value of undefined           
// make sure this variable isn't declared 
// let ageUndefinedOne           
console.log(typeof messageUndefinedFour);  // "undefined" 
console.log(typeof ageUndefinedOne);      // "undefined"

let messageUndefinedFive;   // this variable is declared but has a value of undefined 
// 'ageUndefinedTwo' is not declared
 
if (messageUndefinedFive) {  // This block will not execute 
}
 
if (!messageUndefinedFive) {  // This block will execute 
}
 
/* if (ageUndefinedTwo) {  // This will throw an error 
} */

//The Null Type
let car = null; 
console.log(typeof car);   // "object"
//The value undefined is a derivative of null
console.log(null == undefined);   // true 

let messageNull = null; let ageNull;
 
if (messageNull) {  // This block will not execute 
}
 
if (!messageNull) {  // This block will execute 
}
 
if (ageNull) {  // This block will not execute 
}
 
if (!ageNull) {  // This block will execute 
}

//The Boolean Type
let foundBoolean = true; 
let lostBoolean = false; 

let stringMessage = "Hello world!"; 
let messageAsBoolean = Boolean(stringMessage);

/*
DATA TYPE     VALUES CONVERTED TO TRUE                           VALUES CONVERTED TO FALSE
Boolean         true                                              false
String          Any nonempty string                               "" (empty string)
Number          Any nonzero number (including infinity)           0, NaN (See the “NaN” section later in this chapter.)
Object          Any object                                        null
Undefined       n/a                                               undefined
*/


// The Number Type 

let intNum = 55;  // integer
let octalNum1 = 070;  // octal for 56 
let octalNum2 = 079;  // invalid octal - interpreted as 79 
let octalNum3 = 08;   // invalid octal - interpreted as 8

let hexNum1 = 0xA;   // hexadecimal for 10 
let hexNum2 = 0x1f;  // hexadecimal for 31 

//Floating-Point Values 

let floatNum1 = 1.1; 
let floatNum2 = 0.1; 
let floatNum3 = .1;   // valid, but not recommended
let floatNum4 = 1.;    // missing digit after decimal - interpreted as integer 1 
let floatNum5 = 10.0;  // whole number - interpreted as integer 10
let floatNum = 3.125e7;  // equal to 31250000


//Range of Values
console.log(Number.MIN_VALUE);
console.log(Number.MAX_VALUE);

let result = Number.MAX_VALUE + Number.MAX_VALUE; 
console.log(result);
console.log(isFinite(result));  // false 


//NaN - Not a Number - which is used to indicate when an operation intended to return a number has failed (as opposed to throwing an error)
console.log(NaN == NaN);  // false
console.log(isNaN(NaN));     // true 
console.log(isNaN(10));      // false - 10 is a number 
console.log(isNaN("10"));    // false - can be converted to number 10 
console.log(isNaN("blue"));  // true - cannot be converted to a number 
console.log(isNaN(true));    // false - can be converted to number 1

//####################################################################

//Number Conversions

let num1 = Number("Hello world!");  // NaN 
let num2 = Number("");              // 0
let num3 = Number("000011");        // 11 
let num4 = Number(true);            // 1 

let num5 = parseInt("1234blue");  // 1234 
let num6 = parseInt("");          // NaN 
let num7 = parseInt("0xA");       // 10 - hexadecimal 
let num8 = parseInt(22.5);        // 22 
let num9 = parseInt("70");        // 70 - decimal 
let num10 = parseInt("0xf");       // 15 - hexadecimal 

let num11 = parseInt("0xAF", 16);     // 175
let num12 = parseInt("AF", 16);  // 175 
let num13 = parseInt("AF");   // NaN
let num14 = parseInt("10", 2);   // 2 - parsed as binary 
let num15 = parseInt("10", 8);   // 8 - parsed as octal 
let num16 = parseInt("10", 10);  // 10 - parsed as decimal 
let num17 = parseInt("10", 16);  // 16 - parsed as hexadecimal

//parseFloat
let float1 = parseFloat("1234blue");  // 1234 - integer 
let float2 = parseFloat("0xA");       // 0 
let float3 = parseFloat("22.5");      // 22.5 
let float4 = parseFloat("22.34.5");   // 22.34 
let float5 = parseFloat("0908.5");    // 908.5 
let float6 = parseFloat("3.125e7");   // 31250000

//The String Type

let firstName1 = "John"; 
let lastName1 = 'Jacob'; 
let lastName2 = `Jingleheimerschmidt`
let firstName2 = 'Nicholas";  // syntax error - quotes must match

//The Nature of Strings  - Strings are immutable
let lang = "Java"; 
lang = lang + "Script";

//Converting to a String

let age = 11; 
let ageAsString = age.toString();      // the string "11" 
let found = true; 
let foundAsString = found.toString();  // the string "true"

// The toString() method is available on values that are numbers, Booleans, objects, and strings

let num = 10; 
console.log(num.toString());     // "10" 
console.log(num.toString(2));    // "1010" 
console.log(num.toString(8));    // "12" 
console.log(num.toString(10));   // "10" 
console.log(num.toString(16));   // "a"

let value1 = 10; 
let value2 = true; 
let value3 = null; 
let value4;           
console.log(String(value1));   // "10" 
console.log(String(value2));   // "true" 
console.log(String(value3));   // "null" 
console.log(String(value4));   // "undefined" 

//Template Literals - New in ECMAScript 6 
let myMultiLineString = 'first line\nsecond line'; 
let myMultiLineTemplateLiteral = `first line 
second line`;
 
console.log(myMultiLineString);  
// first line 
// second line"

console.log(myMultiLineTemplateLiteral);   
// first line 
// second line
 
console.log(myMultiLineString === myMultiLinetemplateLiteral);   // true

let pageHTML = ` <div>  
                  <a href="#">    
                    <span>Jake</span>  
                  </a> 
                </div>`;

//Interpolation 
let value = 5; 
let exponent = 'second';
 
// Formerly, interpolation was accomplished as follows: 
let interpolatedString =   value + ' to the ' + exponent + ' power is ' + (value * value);

// The same thing accomplished with template literals: 
let interpolatedTemplateLiteral =   `${ value } to the ${ exponent } power is ${ value * value }`;
 
console.log(interpolatedString);           // 5 to the second power is 25 
console.log(interpolatedTemplateLiteral);  // 5 to the second power is 25 

//Template Literal Tag Functions

let a = 6; 
let b = 9;

function simpleTag(strings, aValExpression, bValExpression, sumExpression) {  
  console.log(strings);  
  console.log(aValExpression);  
  console.log(bValExpression);  
  console.log(sumExpression);
 
  return 'foobar'; 
}
 
let untaggedResult = `${ a } + ${ b } = ${ a + b }`; 
let taggedResult = simpleTag`${ a } + ${ b } = ${ a + b }`; 
// ["", " + ", " = ", ""] 
// 6 
// 9 
// 15
 
console.log(untaggedResult);  // "6 + 9 = 15" 
console.log(taggedResult);    // "foobar" 

let a = 6; 
let b = 9;
 
function simpleTag(strings, ...expressions) {  
  console.log(strings);  
  for(const expression of expressions) {    
    console.log(expression);  
  }
 
  return 'foobar'; 
} 

let taggedResult = simpleTag`${ a } + ${ b } = ${ a + b }`; 
// ["", " + ", " = ", ""] 
// 6 
// 9 
// 15
 
console.log(taggedResult);  // "foobar" 

//Simple function
function nameFunction() {
    console.log("hello world!");
}

function nameFunction(variable1) {
  console.log(variable1);
}

function nameFunction(variable1, variable2) {
  console.log(variable1, variable2);
}

//in javascript doesnt exist the overload methods
nameFunction();
nameFunction("hello 2");
nameFunction("hello 2", "hello 3");


//fat arrow function

let myArrowFunction = (firstVariable, secondVariable) => {
    console.log(firstVariable, secondVariable)
}
let mySecondArrow = myVariable => console.log(myVariable);

let mySecondFunctionVariable = function secondFunction() {
  return "hello Wilson";
}

//callback

let imprimirDos = (apellido) => console.log(apellido);

let imprimirSaludarDos = (name, callback) => {
  console.log(name);
  callback();
}

//The Symbol Type

let sym = Symbol();
console.log(typeof sym);
